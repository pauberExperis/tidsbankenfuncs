﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace TidsbankenFuncs.Migrations
{
    public partial class manyid : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Id",
                table: "userIneligiblePeriod");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "comment");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "userIneligiblePeriod",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "comment",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }
    }
}
