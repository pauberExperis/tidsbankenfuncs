﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace TidsbankenFuncs.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "settings",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Value = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_settings", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "user",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    ProfilePic = table.Column<string>(nullable: true),
                    IsAdmin = table.Column<bool>(nullable: false),
                    FirstName = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Email = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_user", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "moderator",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_moderator", x => x.Id);
                    table.ForeignKey(
                        name: "FK_moderator_user_UserId",
                        column: x => x.UserId,
                        principalTable: "user",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ineligiblePeriod",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PeriodStart = table.Column<DateTime>(nullable: false),
                    PeriodEnd = table.Column<DateTime>(nullable: false),
                    ModeratorId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ineligiblePeriod", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ineligiblePeriod_moderator_ModeratorId",
                        column: x => x.ModeratorId,
                        principalTable: "moderator",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "vacationRequest",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(nullable: true),
                    PeriodStart = table.Column<DateTime>(nullable: false),
                    PeriodEnd = table.Column<DateTime>(nullable: false),
                    TimeOfModeration = table.Column<DateTime>(nullable: false),
                    UserId = table.Column<int>(nullable: false),
                    ModeratorId = table.Column<int>(nullable: true),
                    Status = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_vacationRequest", x => x.Id);
                    table.ForeignKey(
                        name: "FK_vacationRequest_moderator_ModeratorId",
                        column: x => x.ModeratorId,
                        principalTable: "moderator",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_vacationRequest_user_UserId",
                        column: x => x.UserId,
                        principalTable: "user",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "userIneligiblePeriod",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    IneligiblePeriodId = table.Column<int>(nullable: false),
                    Id = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_userIneligiblePeriod", x => new { x.UserId, x.IneligiblePeriodId });
                    table.ForeignKey(
                        name: "FK_userIneligiblePeriod_ineligiblePeriod_IneligiblePeriodId",
                        column: x => x.IneligiblePeriodId,
                        principalTable: "ineligiblePeriod",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_userIneligiblePeriod_user_UserId",
                        column: x => x.UserId,
                        principalTable: "user",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "comment",
                columns: table => new
                {
                    UserId = table.Column<int>(nullable: false),
                    VacationRequestId = table.Column<int>(nullable: false),
                    Id = table.Column<int>(nullable: false),
                    Message = table.Column<string>(nullable: true),
                    Created = table.Column<DateTime>(nullable: false),
                    LastEdited = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_comment", x => new { x.UserId, x.VacationRequestId });
                    table.ForeignKey(
                        name: "FK_comment_user_UserId",
                        column: x => x.UserId,
                        principalTable: "user",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_comment_vacationRequest_VacationRequestId",
                        column: x => x.VacationRequestId,
                        principalTable: "vacationRequest",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_comment_VacationRequestId",
                table: "comment",
                column: "VacationRequestId");

            migrationBuilder.CreateIndex(
                name: "IX_ineligiblePeriod_ModeratorId",
                table: "ineligiblePeriod",
                column: "ModeratorId");

            migrationBuilder.CreateIndex(
                name: "IX_moderator_UserId",
                table: "moderator",
                column: "UserId",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_user_Email",
                table: "user",
                column: "Email",
                unique: true,
                filter: "[Email] IS NOT NULL");

            migrationBuilder.CreateIndex(
                name: "IX_userIneligiblePeriod_IneligiblePeriodId",
                table: "userIneligiblePeriod",
                column: "IneligiblePeriodId");

            migrationBuilder.CreateIndex(
                name: "IX_vacationRequest_ModeratorId",
                table: "vacationRequest",
                column: "ModeratorId");

            migrationBuilder.CreateIndex(
                name: "IX_vacationRequest_UserId",
                table: "vacationRequest",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "comment");

            migrationBuilder.DropTable(
                name: "settings");

            migrationBuilder.DropTable(
                name: "userIneligiblePeriod");

            migrationBuilder.DropTable(
                name: "vacationRequest");

            migrationBuilder.DropTable(
                name: "ineligiblePeriod");

            migrationBuilder.DropTable(
                name: "moderator");

            migrationBuilder.DropTable(
                name: "user");
        }
    }
}
