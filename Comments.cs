﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using TidsbankenFuncs.Models;
using System.Net.Http;
using System.Security.Claims;

namespace TidsbankenFuncs
{
    public class Comments
    {
        private const string Route = "request/{request_id}/comment";
        private readonly System.Net.Http.HttpClient comClient;
        private readonly TidsbankenFuncsDbContext comContext;
        static List<Comment> items = new List<Comment>();
        static List<Object> results = new List<Object>();

        public Comments(IHttpClientFactory httpClientFactory, TidsbankenFuncsDbContext comContext)
        {
            this.comClient = httpClientFactory.CreateClient();
            this.comContext = comContext;
        }

        [FunctionName("CreateComment")]
        public async Task<IActionResult> CreateComment([HttpTrigger(AuthorizationLevel.Anonymous,
            "post", Route = Route)]HttpRequest req, int request_id, ILogger log, ClaimsPrincipal claimsPrincipal)//, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            bool userIsCorrect = controlUser.idMatchingDbId(comContext, Int32.Parse(req.Headers["UserId"]));
            bool isAdmin = controlUser.isUserAdmin(comContext);

            if (!userIsCorrect && !isAdmin) // If a non-admin tries to create comments on request is does not own
            {
                log.LogInformation("UserId input in request does not match user making request");
                return new StatusCodeResult(403);
            }

            log.LogInformation("Creating a new comment");
            var com = new Comment
            {
                UserId = Int32.Parse(req.Headers["UserId"]),
                VacationRequestId = request_id,
                Message = req.Headers["Message"],
                Created = DateTime.UtcNow.ToLocalTime()
            };

            await comContext.comment.AddAsync(com);
            await comContext.SaveChangesAsync();
            return new CreatedResult(Route + "/" + com.UserId, controlUser.serializeObject<Comment>(com));
        }
            
        [FunctionName("GetComment")]
        public async Task<IActionResult> GetComment([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = Route)]HttpRequest req, ILogger log, int request_id, ClaimsPrincipal claimsPrincipal)//, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            User currentUser = comContext.user.Where(u => u.Email == controlUser.epost).FirstOrDefault();
            bool isAdmin = controlUser.isUserAdmin(comContext);
            log.LogInformation("Getting comment list items");
            if (isAdmin)
            {
                items = await comContext.comment.Include("User").Include("VacationRequest")
                    .Where(c => c.VacationRequestId == request_id).ToListAsync();
            }
            else if (comContext.vacationRequest.Where(vr=> vr.Id == request_id).FirstOrDefault().UserId == currentUser.Id)
            {
                items = await comContext.comment.Include("User").Include("VacationRequest")
                    .Where(c => (c.VacationRequestId == request_id)).ToListAsync();
            } else
            {
                return new StatusCodeResult(403);
            }
            log.LogInformation("Checking if items is null");
            if (items == null)
            {
                return new NoContentResult();
            } else {
                log.LogInformation("Items is not null");
            }
            log.LogInformation("Checking if first index is null: " +items.Count );
             if (items.Count == 0)
            {
                log.LogInformation("Item is null");
                return new NoContentResult();
            } else
            {
                log.LogInformation("Item was not null");
            }
            log.LogInformation("Going through comments");
            foreach(Comment com in items)
            {
                com.VacationRequest.Comments = null;
                com.User = null;
                com.VacationRequest.User = null;
                if (com.VacationRequest.Moderator != null) 
                com.VacationRequest.Moderator = null;
            }
            return new OkObjectResult(controlUser.serializeObjectList<Comment>(items));

        }

        [FunctionName("GetCommentById")]
        public IActionResult GetCommentById([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = Route + "/{comment_id}")]HttpRequest req, ILogger log, int request_id, int comment_id, 
            ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            User currentUser = comContext.user.Where(u => u.Email == controlUser.epost).FirstOrDefault();
            bool isAdmin = controlUser.isUserAdmin(comContext);
            log.LogInformation("Getting comment list items");
            
            if (isAdmin)
            {
                Comment com = comContext.comment.Include("User").Include("VacationRequest")
                    .Where(c => (c.VacationRequestId == request_id && c.UserId == comment_id)).FirstOrDefault();
                if (com == null)
                {
                    return new NoContentResult();
                }
                return new OkObjectResult(controlUser.serializeObject<Comment>(com));
            }
            else if (comContext.vacationRequest.Where(vr => vr.Id == request_id).FirstOrDefault().UserId == currentUser.Id)
            {
                Comment com = comContext.comment.Include("User").Include("VacationRequest")
                    .Where(c => (c.UserId == currentUser.Id && c.VacationRequestId == request_id && c.UserId == comment_id)).FirstOrDefault();
                if (com == null)
                {
                    return new NoContentResult();
                }
                return new OkObjectResult(controlUser.serializeObject<Comment>(com));
            }
            else
            {
                return new StatusCodeResult(403);
            }
        }
        [FunctionName("UpdateComment")]
        public async Task<IActionResult> UpdateComment([HttpTrigger(AuthorizationLevel.Anonymous, "patch",
            Route = Route + "/{comment_id}")]HttpRequest req, ILogger log, int comment_id, int request_id, 
            ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            User currentUser = comContext.user.Where(u => u.Email == controlUser.epost).FirstOrDefault();
            DateTime now = DateTime.UtcNow.ToLocalTime();
            Comment com = comContext.comment.Include("User").Include("VacationRequest").Where(c => (c.UserId == comment_id && c.VacationRequestId == request_id)).FirstOrDefault();
            if (com == null)
            {
                return new NoContentResult();
            }
            if (currentUser.Id != com.UserId)
            {
                log.LogInformation("UserId input in request does not match user making request");
                return new StatusCodeResult(403);
            }
            DateTime expireTime = com.Created.AddDays(1);
            if (now > expireTime)
            {
                return new BadRequestObjectResult("You can only edit comments within the first 24 hours");
            }
            else
            {
                com.Message = req.Headers["Message"];
                com.LastEdited = now;

                var dbEntityEntry = comContext.Entry(com);
                await comContext.SaveChangesAsync();

                return new OkObjectResult(controlUser.serializeObject<Comment>(com));
            }
        }

        [FunctionName("DeleteComment")]
        public async Task<IActionResult> DeleteComment([HttpTrigger(AuthorizationLevel.Anonymous, "delete",
            Route = Route + "/{comment_id}")]HttpRequest req, ILogger log, int comment_id, int request_id,
            ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            User currentUser = comContext.user.Where(u => u.Email == controlUser.epost).FirstOrDefault();
            DateTime now = DateTime.UtcNow.ToLocalTime();
            Comment com = comContext.comment.Include("User").Include("VacationRequest").Where(c => c.UserId == comment_id).FirstOrDefault();
            if (com == null)
            {
                return new NoContentResult();
            }
            if (currentUser.Id != com.UserId)
            {
                log.LogInformation("UserId input in request does not match user making request");
                return new StatusCodeResult(403);
            }
            DateTime expireTime = com.Created.AddDays(1);
            if (now > expireTime)
            {
                return new BadRequestObjectResult("You can only delete comments within the first 24 hours");
            }
            else
            {
                comContext.comment.Remove(com);
                await comContext.SaveChangesAsync();
                return new OkObjectResult("Comment with ID: " + com.UserId + " has been deleted");
            }
        }
    }
}
