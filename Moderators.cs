﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using TidsbankenFuncs.Models;
using System.Net.Http;

namespace TidsbankenFuncs
{
    public class Moderators
    {
        private const string Route = "moderator";
        private readonly System.Net.Http.HttpClient modClient;
        private readonly TidsbankenFuncsDbContext modContext;
        static List<Moderator> items = new List<Moderator>();
        static List<Object> results = new List<Object>();
        //static string connectionString = System.Environment.GetEnvironmentVariable("CONNSTR_Tidsbanken");

        public Moderators(IHttpClientFactory httpClientFactory, TidsbankenFuncsDbContext modContext)
        {
            this.modClient = httpClientFactory.CreateClient();
            this.modContext = modContext;
        }

        /*[FunctionName("CreateMod")]
        public async Task<IActionResult> CreateUser([HttpTrigger(AuthorizationLevel.Anonymous,
            "post", Route = Route)]HttpRequest req, ILogger log)
        {
            log.LogInformation("Creating a new user");
            var requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var input = JsonConvert.DeserializeObject<User>(requestBody);
            var user = new User
            {
                ProfilePic = input.ProfilePic,
                IsAdmin = input.IsAdmin,
                FirstName = input.FirstName,
                LastName = input.LastName,
                Email = input.Email
            };

            await userContext.user.AddAsync(user);
            await userContext.SaveChangesAsync();
            return new OkObjectResult(user);
            /*string variable = "reader: ";
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                await conn.OpenAsync();
                log.LogInformation($"--> SQL Connection Opened {DateTime.Now}");
                string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
                var input = JsonConvert.DeserializeObject<Models.User>(requestBody);
                var insertCommand = $"insert into [User](ProfilePic, IsAdmin, FirstName, LastName, Email) output INSERTED.ID values ('{input.ProfilePic}', '{input.IsAdmin}', '{input.FirstName}', '{input.LastName}', '{input.Email}');";
                using (SqlCommand cmdInsert = new SqlCommand(insertCommand, conn))
                {
                    int modified = (int)cmdInsert.ExecuteScalar();
                    log.LogInformation($"user ID of {input.Email} is " + modified);
                    //return new OkObjectResult($"user ID of {input.Email} is " + modified);
                    return new CreatedResult($"user ID of {input.Email} is ", modified);
                    //await cmdInsert.ExecuteNonQueryAsync
                }
            }

            
            
            return new OkResult();


        }*/

        [FunctionName("GetModerator")]
        public async Task<IActionResult> GetModerator([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = Route)]HttpRequest req, ILogger log)
        {
            log.LogInformation("Getting moderator list items");
            items = await modContext.moderator./*Include("Comments").Include("Moderator").Include("UserIneligiblePeriods").Include("VacationRequests").*/ToListAsync();
            return new OkObjectResult(JsonConvert.SerializeObject(items, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
            }
            ));
            /*List<Models.User> items = new List<Models.User>();
            log.LogInformation("Getting user list items");
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                await conn.OpenAsync();
                var selectQuery = $"select * from [User]";
                using (SqlCommand cmd = new SqlCommand(selectQuery, conn))
                {
                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        while (reader.Read())
                        {
                            string id = reader["Id"].ToString();
                            string profilePic = reader["ProfilePic"].ToString();
                            string isAdmin = reader["IsAdmin"].ToString();
                            string email = reader["Email"].ToString();
                            string firstName = reader["FirstName"].ToString();
                            string lastName = reader["LastName"].ToString();
                            items.Add(new Models.User { Id = Int32.Parse(id), ProfilePic = profilePic, IsAdmin = bool.Parse(isAdmin), Email = email, FirstName = firstName, LastName = lastName });
                      
                        }
                    }
                    
                }
            }
                foreach(var item in items)
            {
                log.LogInformation(item.Id.ToString() + " " + item.ProfilePic + " " + item.IsAdmin.ToString());
            }
                return new OkObjectResult(items); */
        }

        [FunctionName("GetModeratorById")]
        public IActionResult GetModeratorById([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = "moderator/{id}")]HttpRequest req, ILogger log, int id)
        {
            Moderator mod = modContext.moderator.Where(u => u.Id == id).FirstOrDefault();
            if (mod == null)
            {
                return new NotFoundResult();
            }
            return new OkObjectResult(JsonConvert.SerializeObject(mod, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
            }
            ));
        }
        /*[FunctionName("UpdateUser")]
        public async Task<IActionResult> UpdateUser([HttpTrigger(AuthorizationLevel.Anonymous, "patch",
            Route = "user/{id}")]HttpRequest req, ILogger log, int id)
        {
            User user = userContext.user.Where(u => u.Id == id).FirstOrDefault();
            //items.FirstOrDefault(t => t.Id == id);
            if (user == null)
            {
                return new NotFoundResult();
            }

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var updated = JsonConvert.DeserializeObject<Models.User>(requestBody);
            if (user.IsAdmin == false && updated.IsAdmin == true)
            {
                Moderator mod = new Moderator { UserId = user.Id };
                await userContext.moderator.AddAsync(mod);

                // await userContext.SaveChangesAsync();
            }
            else if (user.IsAdmin == true && updated.IsAdmin == false)
            {
                Moderator mod = userContext.moderator.Where(m => m.UserId == user.Id).FirstOrDefault();
                if (mod != null)
                {
                    userContext.moderator.Remove(mod);
                }
                //await userContext.SaveChangesAsync();
            }
            user.IsAdmin = updated.IsAdmin;
            if (!string.IsNullOrEmpty(updated.ProfilePic))
            {
                user.ProfilePic = updated.ProfilePic;
            }
            if (!string.IsNullOrEmpty(updated.LastName))
            {
                user.LastName = updated.LastName;
            }
            if (!string.IsNullOrEmpty(updated.FirstName))
            {
                user.FirstName = updated.FirstName;
            }
            if (!string.IsNullOrEmpty(updated.Email))
            {
                user.Email = updated.Email;
            }
            int lolId = user.Id;
            var dbEntityEntry = userContext.Entry(user);
            await userContext.SaveChangesAsync();
            //await modContext.SaveChangesAsync();




            return new OkObjectResult(user);
        }

        [FunctionName("DeleteUser")]
        public async Task<IActionResult> DeleteUser([HttpTrigger(AuthorizationLevel.Anonymous, "delete",
            Route = "user/{id}")]HttpRequest req, ILogger log, int id)
        {
            User user = userContext.user.Where(u => u.Id == id).FirstOrDefault();
            if (user == null)
            {
                return new NotFoundResult();
            }
            userContext.user.Remove(user);
            await userContext.SaveChangesAsync();
            return new OkObjectResult("User with ID: " + user.Id + " has been deleted");
        }

        /*public async void AddModerator(int id)
        {
            Moderator mod = new Moderator { UserId = id };
            await modContext.moderator.AddAsync(mod);
            await modContext.SaveChangesAsync();
        }*/

    }
}
