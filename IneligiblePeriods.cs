﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using TidsbankenFuncs.Models;
using System.Net.Http;
using System.Security.Claims;

namespace TidsbankenFuncs
{
    public class IneligiblePeriods
    {
        private const string Route = "ineligible";
        private readonly System.Net.Http.HttpClient ineClient;
        private readonly TidsbankenFuncsDbContext ineContext;
        static List<IneligiblePeriod> items = new List<IneligiblePeriod>();
        static List<Object> results = new List<Object>();

        public IneligiblePeriods(IHttpClientFactory httpClientFactory, TidsbankenFuncsDbContext ineContext)
        {
            this.ineClient = httpClientFactory.CreateClient();
            this.ineContext = ineContext;
        }

        [FunctionName("CreateIneligiblePeriods")]
        public async Task<IActionResult> CreateIneligiblePeriods([HttpTrigger(AuthorizationLevel.Anonymous,
            "post", Route = Route)]HttpRequest req, ILogger log, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            bool isAdmin = controlUser.isUserAdmin(ineContext);

            if (!isAdmin) // If a non-admin tries to create IneligeblePeriod
            {
                log.LogInformation("Non-admin making request");
                return new StatusCodeResult(403);
            }
            log.LogInformation(req.Headers["ModeratorId"]);

            log.LogInformation("Creating a new Ineligible Period");
            var ine = new IneligiblePeriod
            {
                ModeratorId = Int32.Parse(req.Headers["ModeratorId"]),
                PeriodStart = DateTime.Parse(req.Headers["PeriodStart"]),
                PeriodEnd = DateTime.Parse(req.Headers["PeriodEnd"])
            };

            await ineContext.ineligiblePeriod.AddAsync(ine);
            await ineContext.SaveChangesAsync();
            return new CreatedResult(Route + "/" + ine.Id, controlUser.serializeObject<IneligiblePeriod>(ine));
        }

        [FunctionName("GetIneligiblePeriod")]
        public async Task<IActionResult> GetIneligiblePeriod([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = Route)]HttpRequest req, ILogger log, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            //User currentUser = ineContext.user.Where(u => u.Email == controlUser.epost).FirstOrDefault();
            //bool isAdmin = controlUser.isUserAdmin(ineContext);
            log.LogInformation("Getting ineligible period list items");
            items = await ineContext.ineligiblePeriod.ToListAsync();
            return new OkObjectResult(controlUser.serializeObjectList<IneligiblePeriod>(items));
            /*if (isAdmin)
            {
               return new OkObjectResult(controlUser.serializeObjectList<IneligiblePeriod>(items));
            } else
            {
                List<IneligiblePeriod> userIp = new List<IneligiblePeriod>();
                foreach(IneligiblePeriod ip in items)
                {
                    foreach (UserIneligiblePeriod uip in ip.UserIneligiblePeriods)
                    {
                        if (uip.UserId == currentUser.Id)
                        {
                            userIp.Add(ip);
                        }
                    }
                    
                }
                return new OkObjectResult(controlUser.serializeObjectList<IneligiblePeriod>(userIp));
            }*/


        }

        [FunctionName("GetIneligiblePeriodById")]
        public IActionResult GetIneligiblePeriodById([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = Route + "/{ip_id}")]HttpRequest req, ILogger log, int ip_id)
        {
            IneligiblePeriod ine = ineContext.ineligiblePeriod
                .Where(u => (u.Id == ip_id)).FirstOrDefault();
            if (ine == null)
            {
                return new NoContentResult();
            }
            return new OkObjectResult(JsonConvert.SerializeObject(ine, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
            }
            ));
        }
        [FunctionName("UpdateIneligiblePeriod")]
        public async Task<IActionResult> UpdateIneligiblePeriod([HttpTrigger(AuthorizationLevel.Anonymous, "patch",
            Route = Route + "/{ip_id}")]HttpRequest req, ILogger log, int ip_id, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            bool isAdmin = controlUser.isUserAdmin(ineContext);

            if (!isAdmin) // If a non-admin tries to create IneligeblePeriod
            {
                log.LogInformation("Non-admin making request");
                return new StatusCodeResult(403);
            }

            IneligiblePeriod ine = ineContext.ineligiblePeriod.Include("UserIneligiblePeriods")
                .Where(u => u.Id == ip_id).FirstOrDefault();
            //items.FirstOrDefault(t => t.Id == id);
            if (ine == null)
            {
                return new NoContentResult();
            }

            ine.PeriodEnd = DateTime.Parse(req.Headers["PeriodEnd"]);
            ine.PeriodStart = DateTime.Parse(req.Headers["PeriodStart"]);
            ine.ModeratorId = Int32.Parse(req.Headers["ModeratorId"]);

            var dbEntityEntry = ineContext.Entry(ine);
            await ineContext.SaveChangesAsync();

            return new OkObjectResult(controlUser.serializeObject<IneligiblePeriod>(ine));
        }

        [FunctionName("DeleteIneligiblePeriod")]
        public async Task<IActionResult> DeleteIneligiblePeriod([HttpTrigger(AuthorizationLevel.Anonymous, "delete",
            Route = Route + "/{ip_id}")]HttpRequest req, ILogger log, int ip_id, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            bool isAdmin = controlUser.isUserAdmin(ineContext);

            if (!isAdmin) // If a non-admin tries to delete IneligeblePeriod
            {
                log.LogInformation("Non-admin making request");
                return new StatusCodeResult(403);
            }

            IneligiblePeriod ine = ineContext.ineligiblePeriod.Include("UserIneligiblePeriods").Where(u => u.Id == ip_id).FirstOrDefault();

            if (ine == null)
            {
                return new NoContentResult();
            }
            foreach(UserIneligiblePeriod userIne in ine.UserIneligiblePeriods)
            {
                ineContext.userIneligiblePeriod.Remove(userIne);
            }
            ineContext.ineligiblePeriod.Remove(ine);
            await ineContext.SaveChangesAsync();

            return new OkObjectResult("IneligiblePeriod with ID: " + ine.Id + " has been deleted");
        }
    }
}
