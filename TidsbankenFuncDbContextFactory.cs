﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace TidsbankenFuncs
{
    class TidsbankenFuncDbContextFactory: IDesignTimeDbContextFactory<TidsbankenFuncsDbContext>
    {
        public TidsbankenFuncsDbContext CreateDbContext(string[] args)
        {
            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("local.settings.json", optional: true)
                .Build();
            var connParameter = "SqlConnectionString";
            var Prefix = "SQLAZURECONNSTR_";
            var conn = configuration.GetConnectionString("SqlConnectionString");
            if (string.IsNullOrEmpty(conn))
            {
                conn = System.Environment.GetEnvironmentVariable($"SQLAZURECONNSTR_SqlConnectionString", EnvironmentVariableTarget.Process);
            }
            var connectionString = System.Environment.GetEnvironmentVariable($"ConnectionStrings:{connParameter}", EnvironmentVariableTarget.Process);
            if (string.IsNullOrEmpty(connectionString))
            {
                connectionString = System.Environment.GetEnvironmentVariable($"{Prefix}{connParameter}", EnvironmentVariableTarget.Process);
            }
            var optionsBuilder = new DbContextOptionsBuilder<TidsbankenFuncsDbContext>();
            optionsBuilder.UseSqlServer(conn);
            return new TidsbankenFuncsDbContext(optionsBuilder.Options);
        }

    }
    
}
