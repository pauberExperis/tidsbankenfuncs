using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using TidsbankenFuncs.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Security.Claims;
using PasswordGenerator;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Text;

namespace TidsbankenFuncs
{
    public class Users
    {
        private const string Route = "user";
        private readonly System.Net.Http.HttpClient userClient;
        private readonly TidsbankenFuncsDbContext userContext;
        private readonly TidsbankenFuncsDbContext userContext2;
        static List<User> items = new List<User>();
        static List<Object> results = new List<Object>();

        //static string connectionString = System.Environment.GetEnvironmentVariable("CONNSTR_Tidsbanken");

        public Users(System.Net.Http.IHttpClientFactory httpClientFactory, TidsbankenFuncsDbContext userContext, TidsbankenFuncsDbContext userContext2)
        {
            this.userClient = httpClientFactory.CreateClient();
            this.userContext = userContext;
            this.userContext2 = userContext2;
        }

        [FunctionName("CreateUser")]
        public async Task<IActionResult> CreateUser([HttpTrigger(AuthorizationLevel.Anonymous,
            "post", Route = Route)]HttpRequest req, ILogger log, ClaimsPrincipal claimsPrincipal)
        {
            // The ControlUserClass script is located in this directory 
            log.LogInformation("Confirming admin status");
            ControlUserClass userControl = new ControlUserClass(claimsPrincipal, log);
            bool isAdmin = userControl.isUserAdmin(userContext);

            // If user making request is not admin in database, request should be denied
            if (!isAdmin)
            {
                return new StatusCodeResult(403);
            }

            log.LogInformation("Creating a new user");
            var graphToken = req.Headers["GraphToken"];
            //Generate random password
            Password pwd = new Password();
            string randomPassword = pwd.Next();
            string userPrincipalName = req.Headers["Email"];
            if (!userPrincipalName.Contains("@tidsbankenfuncs.ninja"))
            {
                userPrincipalName = userControl.replaceSnabelA(userPrincipalName);
                userPrincipalName = $"{userPrincipalName}%23EXT%23@philipauberthotmail.onmicrosoft.com";
            }
            var graphUser = new
            {
                accountEnabled = true,
                surname = $"{req.Headers["LastName"]}",
                givenName = $"{req.Headers["FirstName"]}",
                displayName = $"{req.Headers["FirstName"]} {req.Headers["LastName"]}",
                mailNickname = $"{req.Headers["FirstName"]}_{req.Headers["LastName"]}",
                userPrincipalName = $"{userPrincipalName}",
                passwordProfile = new
                {
                    forceChangePasswordNextSignIn = true,
                    password = randomPassword
                    //password = "BalleKlorin#1"
                }

            };

            log.LogInformation("Posting to MS Graph");
            userClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", graphToken);
            HttpResponseMessage graphResponse = await userClient.PostAsJsonAsync("https://graph.microsoft.com/v1.0/users/", graphUser);
            string successfullAdminMessage = "Not admin user"; //This is changed if the user is assigned to admin role

            if (graphResponse.StatusCode == HttpStatusCode.Unauthorized)
            {
                log.LogInformation("Access to graph denied");
                return new UnauthorizedResult();
            }
            else if (graphResponse.StatusCode == HttpStatusCode.Accepted || graphResponse.StatusCode == HttpStatusCode.Created ||
                graphResponse.StatusCode == HttpStatusCode.OK || graphResponse.StatusCode == HttpStatusCode.NoContent)
            {
                /*if (bool.Parse(req.Headers["IsAdmin"]))
                {
                    //Give admin role in MS Graph
                    var roleAssignmentBody = new
                    {
                        principalId = userControl.returnAzureADObjectId(graphResponse.Headers.Location.ToString()),
                        roleDefinitionId = "e8611ab8-c189-46e8-94e1-60213ab1f814",
                        directoryScopeId = "/",
                        resourceScopes = "/"
                    };
                    HttpResponseMessage roleUpdateResponse = await userClient.PostAsJsonAsync("https://graph.microsoft.com/beta/roleManagement/directory/roleAssignments", roleAssignmentBody);

                    if (roleUpdateResponse.IsSuccessStatusCode)
                    {
                        successfullAdminMessage = "Admin role1 assigned";
                        var roleAssignmentBody2 = new
                        {
                            principalId = userControl.returnAzureADObjectId(graphResponse.Headers.Location.ToString()),
                            roleDefinitionId = "fe930be7-5e62-47db-91af-98c3a49a38b1",
                            directoryScopeId = "/",
                            resourceScopes = "/"
                        };
                        HttpResponseMessage roleUpdateResponse2 = await userClient.PostAsJsonAsync("https://graph.microsoft.com/beta/roleManagement/directory/roleAssignments", roleAssignmentBody2);

                        if (roleUpdateResponse2.IsSuccessStatusCode)
                        {
                            successfullAdminMessage = "Admin role2 assigned";
                        }
                        else
                        {
                            successfullAdminMessage = "Admin role2 assignment failed";
                        }
                    }
                    else
                    {
                        successfullAdminMessage = "Admin role assignment failed";
                    }
                }*/
               // log.LogInformation(successfullAdminMessage);
                log.LogInformation("Posting to Database");
                var dbUser = new User
                {
                    ProfilePic = req.Headers["ProfilePic"],
                    IsAdmin = false,
                    FirstName = req.Headers["FirstName"],
                    LastName = req.Headers["LastName"],
                    Email = req.Headers["Email"],

                };
                var responseUser = new UserWithPwd
                {
                    ProfilePic = req.Headers["ProfilePic"],
                    IsAdmin = false,
                    FirstName = req.Headers["FirstName"],
                    LastName = req.Headers["LastName"],
                    Email = req.Headers["Email"],
                    Pwd = randomPassword
                };
                await userContext.user.AddAsync(dbUser);
                await userContext.SaveChangesAsync();
                responseUser.Id = dbUser.Id;
                return new CreatedResult(randomPassword, userControl.serializeObject(responseUser));
            }
            else
            {
                log.LogInformation(graphResponse.StatusCode.ToString());
                log.LogInformation("Bad request");
                return new BadRequestResult();
            }
        }

        [FunctionName("ValidateUser")]
        public async Task<IActionResult> ValidateUser([HttpTrigger(AuthorizationLevel.Anonymous,
            "get", Route = Route)]HttpRequest req, ILogger log, ClaimsPrincipal claimsPrincipal)
        {
            log.LogInformation("Confirming admin status");
            ControlUserClass userControl = new ControlUserClass(claimsPrincipal, log);
            if (userControl.epost == "")
            {
                return new BadRequestResult();
            }
            User user = userContext.user.Include("Moderator").Include("UserIneligiblePeriods")
                .Include("VacationRequests").Where(u => u.Email == userControl.epost).FirstOrDefault();

            if (user != null && user.Email != "")
            {
                return new OkObjectResult(JsonConvert.SerializeObject(user, Formatting.Indented, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    PreserveReferencesHandling = PreserveReferencesHandling.Objects
                }
            ));
            }
            else
            {
                var newUser = new User
                {
                    ProfilePic = "",
                    IsAdmin = false,
                    FirstName = userControl.fornavn,
                    LastName = userControl.etternavn,
                    Email = userControl.epost
                };
                await userContext.user.AddAsync(newUser);
                await userContext.SaveChangesAsync();
                return new CreatedResult(Route, JsonConvert.SerializeObject(newUser, Formatting.Indented, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                    PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
                }
            ));
            }
        }


        [FunctionName("GetUser")]
        public async Task<IActionResult> GetUsers([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = Route + "/getall")]HttpRequest req, ILogger log, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            User currentUser = userContext.user.Where(u => u.Email == controlUser.epost).FirstOrDefault();
            bool isAdmin = controlUser.isUserAdmin(userContext);
            var users = await userContext.user.Include("UserIneligiblePeriods").Include("VacationRequests").Include("Comments").Include("Moderator").ToListAsync();
            foreach (User userr in users)
            {
                if (userr.Comments != null)
                {
                    foreach (Comment com in userr.Comments)
                    {
                        if (com.VacationRequest != null)
                        {
                            com.VacationRequest = null;
                            com.User = null;
                        }
                    }
                }
                if (userr.Moderator != null)
                {
                    userr.Moderator.VacationRequests = null;
                }
                if (userr.VacationRequests != null)
                {
                    foreach (VacationRequest vacreq in userr.VacationRequests)
                    {
                        vacreq.Comments = null;
                        vacreq.Moderator = null;
                        vacreq.User = null;
                    }
                }
            }
            List<User> publicUsers = new List<User>();
            if (!isAdmin)
            {
                foreach (User use in users)
                {
                    if (use.Id != currentUser.Id)
                    {
                        use.Email = "";
                        use.IsAdmin = false;
                        use.Moderator = null;
                        use.UserIneligiblePeriods = null;
                        use.Comments = null;
                        List<VacationRequest> vacList = new List<VacationRequest>();
                        foreach (VacationRequest vac in use.VacationRequests)
                        {
                            if (vac.Status == "Approved")
                            {
                                vacList.Add(vac);
                            }
                        }
                        use.VacationRequests = vacList;
                    }
                    publicUsers.Add(use);
                }
                log.LogInformation("Getting user list items");
                return new OkObjectResult(controlUser.serializeObjectList<User>(publicUsers));
            }
            //userClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", graphToken);
            //var me = await userClient.GetStringAsync("https://graph.microsoft.com/v1.0/me");
            //return new OkObjectResult(me);

            log.LogInformation("Getting user list items");
            return new OkObjectResult(controlUser.serializeObjectList<User>(users));

        }

        [FunctionName("GetUserById")]
        public IActionResult GetUserById([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = "user/{user_id}")]HttpRequest req, ILogger log, int user_id, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            bool userIsCorrect = controlUser.idMatchingDbId(userContext, user_id);
            bool isAdmin = controlUser.isUserAdmin(userContext);
            User user = userContext.user.Include("Moderator").Include("UserIneligiblePeriods")
                .Include("VacationRequests").Where(u => u.Id == user_id).FirstOrDefault();
            if (user == null)
            {
                return new NoContentResult();
            }
            if (!userIsCorrect && !isAdmin)
            {
                user.Email = "";
                user.IsAdmin = false;
                user.Moderator = null;
                user.UserIneligiblePeriods = null;
                List<VacationRequest> vacList = new List<VacationRequest>();
                foreach (VacationRequest vac in user.VacationRequests)
                {
                    if (vac.Status == "Approved")
                    {
                        vacList.Add(vac);
                    }
                }
                user.VacationRequests = vacList;

            }


            return new OkObjectResult(controlUser.serializeObject<User>(user));
        }
        [FunctionName("UpdateUser")]
        public async Task<IActionResult> UpdateUser([HttpTrigger(AuthorizationLevel.Anonymous, "patch",
            Route = "user/{user_id}")]HttpRequest req, ILogger log, int user_id, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal, log);
            bool userIsCorrect = controlUser.idMatchingDbId(userContext, user_id);
            bool isAdmin = controlUser.isUserAdmin(userContext);
            string oldUpn = "";
            bool modDelete = false;

            if (!userIsCorrect && !isAdmin) // If a non-admin tries to change another profile than itself
            {
                log.LogInformation("UserId input in request does not match user making request");
                return new StatusCodeResult(403);
            }

            User user = userContext.user.Include("Moderator")
                .Include("UserIneligiblePeriods").Include("VacationRequests").Where(u => u.Id == user_id).FirstOrDefault();
            //items.FirstOrDefault(t => t.Id == id);


            if (user == null)
            {
                log.LogInformation("No user found, user id : " + user_id);
                await Task.Delay(10000);
                user = userContext.user.Include("Moderator")
                .Include("UserIneligiblePeriods").Include("VacationRequests").Where(u => u.Id == user_id).FirstOrDefault();
                if (user == null)
                {
                    log.LogInformation("Still no user found");
                    return new NoContentResult();
                } else
                {
                    log.LogInformation("User found after 10 sec");
                }
            }

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var updated = JsonConvert.DeserializeObject<User>(requestBody);
            bool adminCheck = bool.Parse(req.Headers["IsAdmin"]);

            string userPrincipalName = user.Email;
            if (!userPrincipalName.Contains("@tidsbankenfuncs.ninja"))
            {
                userPrincipalName = controlUser.replaceSnabelA(userPrincipalName);
                userPrincipalName = $"{userPrincipalName}%23EXT%23@philipauberthotmail.onmicrosoft.com";
            }
            log.LogInformation($"https://graph.microsoft.com/v1.0/users/{userPrincipalName}");
            userClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", req.Headers["GraphToken"]);
            var graphUserString = await userClient.GetStringAsync($"https://graph.microsoft.com/v1.0/users/{userPrincipalName}");
            GraphResponse graphUserObject = JsonConvert.DeserializeObject<GraphResponse>(graphUserString);

            if (!user.IsAdmin && adminCheck == true)
            {
                Moderator mod = new Moderator { UserId = user.Id };
                await userContext.moderator.AddAsync(mod);

                //Give admin role in MS Graph
                var roleAssignmentBody = new
                {
                    principalId = graphUserObject.Id,
                    roleDefinitionId = "e8611ab8-c189-46e8-94e1-60213ab1f814",
                    directoryScopeId = "/",
                    resourceScopes = "/"
                };
                HttpResponseMessage roleUpdateResponse = await userClient.PostAsJsonAsync("https://graph.microsoft.com/beta/roleManagement/directory/roleAssignments", roleAssignmentBody);
                log.LogInformation(roleUpdateResponse.StatusCode.ToString());

                
                var roleAssignmentBody2 = new
                {
                    principalId = graphUserObject.Id,
                    roleDefinitionId = "fe930be7-5e62-47db-91af-98c3a49a38b1",
                    directoryScopeId = "/",
                    resourceScopes = "/"
                };
                HttpResponseMessage roleUpdateResponse2 = await userClient.PostAsJsonAsync("https://graph.microsoft.com/beta/roleManagement/directory/roleAssignments", roleAssignmentBody2);

              
                // await userContext.SaveChangesAsync();
            }
            else if (user.IsAdmin && adminCheck == false)
            {
                Moderator mod = userContext.moderator.Include("VacationRequests").Where(m => m.UserId == user.Id).FirstOrDefault();
                if (mod != null)
                {
                    modDelete = true;
                    foreach(VacationRequest modvac in mod.VacationRequests)
                    {
                        autoVacDel(modvac);
                        modvac.Moderator = null;
                        modvac.ModeratorId = null;
                    }
                    userContext.moderator.Remove(mod);
                }

                //Remove admin role in MS Graph
                string reqString = $"https://graph.microsoft.com/beta/roleManagement/directory/roleAssignments?$filter=principalId eq '{graphUserObject.Id}'";
                log.LogInformation(reqString);
                string roleResponseString = await userClient.GetStringAsync(reqString);
                RoleResponse roleResponse = JsonConvert.DeserializeObject<RoleResponse>(roleResponseString);
                string roleId = "";
                string roleId2 = "";
                foreach (Role role in roleResponse.Value)
                {
                    if (role.RoleDefinitionId == "fe930be7-5e62-47db-91af-98c3a49a38b1")
                    {
                        roleId = role.Id;
                    }
                    if (role.RoleDefinitionId == "e8611ab8-c189-46e8-94e1-60213ab1f814")
                    {
                        roleId2 = role.Id;
                    }
                }
                log.LogInformation(roleId);
                if (roleId != "")
                {
                    HttpResponseMessage deleteRespone = await userClient.DeleteAsync($"https://graph.microsoft.com/beta/roleManagement/directory/roleAssignments/{roleId}");
                }
                if (roleId2 != "")
                {
                    HttpResponseMessage deleteRespone = await userClient.DeleteAsync($"https://graph.microsoft.com/beta/roleManagement/directory/roleAssignments/{roleId2}");
                }// else not user administrator

                //await userContext.SaveChangesAsync();
            }
            //New user to patch in MS Graph
            GraphUser graphUser = new GraphUser
            {
                accountEnabled = true,
                surname = user.LastName,
                givenName = user.FirstName,
                displayName = $"{user.FirstName} {user.LastName}",
                mailNickname = $"{user.FirstName}_{user.LastName}",
                userPrincipalName = $"{userPrincipalName}",
            };
            
            user.IsAdmin = adminCheck;
            user.ProfilePic = req.Headers["ProfilePic"];

            if (!string.IsNullOrEmpty(req.Headers["LastName"]))
            {
                user.LastName = req.Headers["LastName"];
                graphUser.surname = req.Headers["LastName"];

                graphUser.displayName = $"{user.FirstName} {req.Headers["LastName"]}";
                graphUser.mailNickname = $"{user.FirstName}_{req.Headers["LastName"]}";
            }
            if (!string.IsNullOrEmpty(req.Headers["FirstName"]))
            {
                user.FirstName = req.Headers["FirstName"];
                graphUser.givenName = req.Headers["FirstName"];

                graphUser.displayName = $"{req.Headers["FirstName"]} {user.LastName}";
                graphUser.mailNickname = $"{req.Headers["FirstName"]}_{user.LastName}";
            }
            if (!string.IsNullOrEmpty(req.Headers["Email"]))
            {
                oldUpn = user.Email;
                user.Email = req.Headers["Email"];
                if (!user.Email.Contains("@tidsbankenfuncs.ninja"))
                {
                    oldUpn = controlUser.replaceSnabelA(oldUpn);
                    graphUser.userPrincipalName = $"{oldUpn}#EXT#@philipauberthotmail.onmicrosoft.com";
                    oldUpn = $"{oldUpn}%23EXT%23@philipauberthotmail.onmicrosoft.com";                 

                }
                else
                {
                    graphUser.userPrincipalName = user.Email;
                }
            }
            //var postGraphUser = new
            //{
            //    surname = graphUser.surname,
            //    givenName = graphUser.givenName,
            //    displayName = graphUser.displayName,
            //    mailNickname = graphUser.mailNickname,
            //    userPrincipalName = graphUser.userPrincipalName,
            //};
            log.LogInformation("Put to MS Graph");
            log.LogInformation($"https://graph.microsoft.com/v1.0/users/{oldUpn}");
            string myJsonString = JsonConvert.SerializeObject(graphUser);
            log.LogInformation(myJsonString);
            HttpContent postGraphUser = new StringContent(myJsonString, Encoding.UTF8, "application/json");
            userClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", req.Headers["GraphToken"]);
            HttpResponseMessage response = await userClient.PatchAsync($"https://graph.microsoft.com/v1.0/users/{oldUpn}", postGraphUser);

            int lolId = user.Id;
            var dbEntityEntry = userContext.Entry(user);
            await userContext.SaveChangesAsync();
            //await modContext.SaveChangesAsync();

            return new OkObjectResult(controlUser.serializeObject<User>(user));
        }

        [FunctionName("GetUserVacationRequest")]
        public async Task<IActionResult> GetUserVacationRequest([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = Route + "/{user_id}" + "/requests")]HttpRequest req, int user_id, ILogger log, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            bool userIsCorrect = controlUser.idMatchingDbId(userContext, user_id);
            bool isAdmin = controlUser.isUserAdmin(userContext);
            List<VacationRequest> vacList = new List<VacationRequest>();
            vacList = await userContext.vacationRequest.Include("Comments").Include("Moderator")
                .Where(vr => vr.UserId == user_id).ToListAsync();
            if (!userIsCorrect && !isAdmin) // If a non-admin tries to delete another profile than itself
            {
                List<VacationRequest> appVacList = new List<VacationRequest>();
                foreach (VacationRequest vac in vacList)
                {
                    if (vac.Status == "Approved")
                    {
                        appVacList.Add(vac);
                    }
                    log.LogInformation("Getting vacation request list items");

                    return new OkObjectResult(controlUser.serializeObjectList<VacationRequest>(appVacList));
                }

            }
            log.LogInformation("Getting vacation request list items");

            return new OkObjectResult(controlUser.serializeObjectList<VacationRequest>(vacList));

        }

        [FunctionName("DeleteUser")]
        public async Task<IActionResult> DeleteUser([HttpTrigger(AuthorizationLevel.Anonymous, "delete",
            Route = "user/{user_id}")]HttpRequest req, ILogger log, int user_id, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            bool userIsCorrect = controlUser.idMatchingDbId(userContext, user_id);
            bool isAdmin = controlUser.isUserAdmin(userContext);

            if (!userIsCorrect && !isAdmin) // If a non-admin tries to delete another profile than itself
            {
                log.LogInformation("UserId input in request does not match user making request");
                return new StatusCodeResult(403);
            }

            User user = userContext.user.Include("VacationRequests").Include("Moderator").Where(u => u.Id == user_id).FirstOrDefault();
            if (user == null)
            {
                return new NoContentResult();
            }
            string userPrincipalName = user.Email;
            if (!userPrincipalName.Contains("@tidsbankenfuncs.ninja"))
            {
                userPrincipalName = controlUser.replaceSnabelA(userPrincipalName);
                userPrincipalName = $"{userPrincipalName}%23EXT%23@philipauberthotmail.onmicrosoft.com";
            }

            userClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", req.Headers["GraphToken"]);
            HttpResponseMessage graphResponse = await userClient.DeleteAsync($"https://graph.microsoft.com/v1.0/users/{userPrincipalName}");
            
            if (graphResponse.StatusCode == HttpStatusCode.Unauthorized)
            {
                log.LogInformation("Access to graph denied");
                return new UnauthorizedResult();
            }

            List<VacationRequest> vacList = await userContext.vacationRequest.Where(v => v.UserId == user_id).ToListAsync();
            foreach (VacationRequest vac in vacList)
            {
                List<Comment> comList = await userContext.comment.Where(c => c.VacationRequest.Id == vac.Id).ToListAsync();
                foreach(Comment com in comList)
                {
                    userContext.comment.Remove(com);
                }
                userContext.vacationRequest.Remove(vac);
                
            }
            if (user.Moderator != null)
            {
                Moderator mod = userContext.moderator.Where(m => m.UserId == user.Id).FirstOrDefault();
                List<VacationRequest> modVacList = await userContext.vacationRequest.Where(vr => vr.ModeratorId == mod.Id).ToListAsync();
                foreach(VacationRequest modVac in modVacList)
                {
                    autoVacDel(modVac);
                }
                userContext.moderator.Remove(mod);
            }
            userContext.user.Remove(user);
            await userContext.SaveChangesAsync();

           
            return new OkObjectResult("User with ID: " + user.Id + " has been deleted");
        }

        [FunctionName("UpdatePassword")]
        public async Task<IActionResult> UpdatePassword([HttpTrigger(AuthorizationLevel.Anonymous,
            "post", Route = Route + "/{user_id}/update_password")]HttpRequest req, ILogger log, int user_id, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            bool userIsCorrect = controlUser.idMatchingDbId(userContext, user_id);

            if (!userIsCorrect) // If a user tries to update another password than its own
            {
                log.LogInformation("UserId input in request does not match user making request");
                return new StatusCodeResult(403);
            }

            if(req.Body == null)
            {
                log.LogInformation("No body with request");
                return new BadRequestResult();
            }
            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var convertedBody = JsonConvert.DeserializeObject<PasswordUpdate>(requestBody);

            var passwordUpdate = new
            {
                currentPassword = convertedBody.Current,
                newPassword = convertedBody.newPass  
            };
            string myJsonString = JsonConvert.SerializeObject(passwordUpdate);
            HttpContent passwordContent = new StringContent(myJsonString, Encoding.UTF8, "application/json");

            userClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", req.Headers["GraphToken"]);
            HttpResponseMessage graphResponse = await userClient.PostAsync("https://graph.microsoft.com/beta/me/changePassword", passwordContent);
            log.LogInformation("Graph response with code:" + graphResponse.StatusCode.ToString());
            
            if (graphResponse.StatusCode == HttpStatusCode.Unauthorized)
            {
                log.LogInformation("Access to graph denied");
                return new UnauthorizedResult();
            }
            else if (graphResponse.StatusCode == HttpStatusCode.Forbidden)
            {
                log.LogInformation("Access to graph forbidden");
                return new StatusCodeResult(403);
            }
            else if (!graphResponse.IsSuccessStatusCode)
            {
                log.LogInformation("Graph problem with code:" + graphResponse.StatusCode.ToString());
                return new StatusCodeResult(400);
            }

            return new OkObjectResult("Password successfully updated");
        }

        public async void autoVacDel(VacationRequest vacationRequest)
        {
            VacationRequest changed = userContext2.vacationRequest.Where(vr => vr.Id == vacationRequest.Id).FirstOrDefault();
            changed.Moderator = null;
            changed.ModeratorId = null;
            var dbEntityEntry = userContext2.Entry(changed);
            await userContext2.SaveChangesAsync();
        }

        /*public async void AddModerator(int id)
        {
            Moderator mod = new Moderator { UserId = id };
            await modContext.moderator.AddAsync(mod);
            await modContext.SaveChangesAsync();
        }*/

    }
}
