﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Security.Claims;
using Microsoft.Extensions.Logging;
using TidsbankenFuncs.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using Newtonsoft.Json;

namespace TidsbankenFuncs
{
    class ControlUserClass
    {
        public string fornavn = "";
        public string etternavn = "";
        public string epost = "";

        /// <summary> Constructor <c>ControlUserClass(ClaimsPrincipal claimsPrincipal, ILogger log)</c> which logs claims</summary>
        public ControlUserClass(ClaimsPrincipal claimsPrincipal, ILogger log)
        {
            foreach (Claim claim in claimsPrincipal.Claims)
            {
                log.LogInformation("TYPE:" + claim.Type + " VALUE: " + claim.Value);
                if (claim.Type.Contains("email") || claim.Type.Contains("claims/upn"))
                {
                    epost = claim.Value;
                }
                if (claim.Type.Contains("surname"))
                {
                    etternavn = claim.Value;
                }
                if (claim.Type.Contains("givenname"))
                {
                    fornavn = claim.Value;
                }
            }
            log.LogInformation(fornavn + " " + etternavn + " " + epost);
        }

        /// <summary> Constructor <c>ControlUserClass(ClaimsPrincipal claimsPrincipal)</c> which does not log claims</summary>
        public ControlUserClass(ClaimsPrincipal claimsPrincipal)
        {
            foreach (Claim claim in claimsPrincipal.Claims)
            {
                if (claim.Type.Contains("email") || claim.Type.Contains("claims/upn"))
                {
                    epost = claim.Value;
                }
                if (claim.Type.Contains("surname"))
                {
                    etternavn = claim.Value;
                }
                if (claim.Type.Contains("givenname"))
                {
                    fornavn = claim.Value;
                }
            }
        }
        
        /// <summary> Method <c>isUserAdmin</c> is returning the isAdmin value of the user in the database matching the user in the claims object</summary>
        public bool isUserAdmin(TidsbankenFuncsDbContext context) 
        {
            User user = context.user.Where(u => u.Email == epost).FirstOrDefault();
            
            return user.IsAdmin;
        }

        /// <summary> Method <c>idMatchingDbId</c> is returning true if the ID in the database matches the input ID</summary>
        public bool idMatchingDbId(TidsbankenFuncsDbContext context, int userId)
        {
            User user = context.user.Where(u => u.Email == epost).FirstOrDefault();

            if(user.Id == userId)
            {
                return true;
            } else
            {
                return false;
            }
        }

        /// <summary> Method <c>userIsRequestOwner</c> returns true if the  userID of the request in the DB matches userID making the request</summary>
        public bool userIsRequestOwner(TidsbankenFuncsDbContext context, int requestId)
        {
            User user = context.user.Where(u => u.Email == epost).FirstOrDefault();
            VacationRequest request = context.vacationRequest.Include("User").Where(u => u.Id == requestId).FirstOrDefault();
            
            if (request.UserId == user.Id)
            { 
                return true;
            }
            
            return false;
        }

        /// <summary> Method <c>userIsCommentOwner</c> returns true if the  userID of the comment in the DB matches userID making the request</summary>
        public bool userIsCommentOwner(TidsbankenFuncsDbContext context, int commentId)
        {
            User user = context.user.Where(u => u.Email == epost).FirstOrDefault();
            Comment comment = context.comment.Include("User").Where(u => u.UserId == commentId).FirstOrDefault();

            if (comment.UserId == user.Id)
            {
                return true;
            }

            return false;
        }
        public string serializeObject<T>(T obj)
        {
            return JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
            }
            );
        }
        public string serializeObjectList<T>(List<T> obj) {
            return JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
            }
            );
        }
        public string returnAzureADObjectId(string location)
        {
            int indexOfStart = location.IndexOf("directoryObjects/")+17;
            int indexOfend = location.IndexOf("/Microsoft.DirectoryServices.User");
            int lengthSubstring = indexOfend - indexOfStart;
            string objectId = location.Substring(indexOfStart, lengthSubstring);
            return objectId;
        }

        public string replaceSnabelA(string someDomain)
        {
            string modifiedString = someDomain.Replace('@', '_');

            return modifiedString;
        }
    }
}
