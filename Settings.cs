﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using TidsbankenFuncs.Models;
using System.Net.Http;
using System.Net.Http.Headers;
using Microsoft.IdentityModel.Clients.ActiveDirectory;
using System.Security.Claims;
using PasswordGenerator;
using System.Net;

namespace TidsbankenFuncs
{
    public class Settings
    {
        private const string Route = "setting";
        private readonly System.Net.Http.HttpClient settingClient;
        private readonly TidsbankenFuncsDbContext settingContext;
        static List<Setting> items = new List<Setting>();

        public Settings(System.Net.Http.IHttpClientFactory httpClientFactory, TidsbankenFuncsDbContext settingContext)
        {
            this.settingClient = httpClientFactory.CreateClient();
            this.settingContext = settingContext;
        }

        [FunctionName("CreateSetting")]
        public async Task<IActionResult> CreateUser([HttpTrigger(AuthorizationLevel.Anonymous,
            "post", Route = Route)]HttpRequest req, ILogger log, ClaimsPrincipal claimsPrincipal)
        {
            // The ControlUserClass script is located in this directory 
            log.LogInformation("Confirming admin status");
            ControlUserClass userControl = new ControlUserClass(claimsPrincipal, log);
            bool isAdmin = userControl.isUserAdmin(settingContext);

            // If user making request is not admin in database, request should be denied
            if (!isAdmin)
            {
                return new StatusCodeResult(403);
            }

            Setting set = new Setting
            {
                Name = $"{req.Headers["Name"]}",
                Value = Int32.Parse($"{req.Headers["Value"]}")
            };
            await settingContext.settings.AddAsync(set);
            await settingContext.SaveChangesAsync();

            return new CreatedResult(Route + "/" + set.Id, JsonConvert.SerializeObject(set, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
            }
            ));
        }



        [FunctionName("GetSettings")]
        public async Task<IActionResult> GetSettings([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = Route)]HttpRequest req, ILogger log)
        {
            var settings = await settingContext.settings.ToListAsync();
            return new OkObjectResult(JsonConvert.SerializeObject(settings, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
            }
            ));

        }

        [FunctionName("GetSettingById")]
        public IActionResult GetSettingById([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = Route + "/{set_id}")]HttpRequest req, ILogger log, int set_id)
        {

            Setting set = settingContext.settings.Where(s => s.Id == set_id).FirstOrDefault();
            if (set == null)
            {
                return new NotFoundResult();
            }
            return new OkObjectResult(JsonConvert.SerializeObject(set, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
            }
            ));
        }
        [FunctionName("UpdateSetting")]
        public async Task<IActionResult> UpdateSetting([HttpTrigger(AuthorizationLevel.Anonymous, "patch",
            Route = Route + "/{set_id}")]HttpRequest req, ILogger log, int set_id, ClaimsPrincipal claimsPrincipal)
        {

            string epost = "";
            foreach (Claim claim in claimsPrincipal.Claims)
            {
                log.LogInformation("TYPE:" + claim.Type + " VALUE: " + claim.Value);
                if (claim.Type.Contains("email"))
                {
                    epost = claim.Value;
                }
            }
            if (epost == "")
            {
                return new StatusCodeResult(403);
            }
            User user = settingContext.user.Include("Moderator").Include("UserIneligiblePeriods").Include("VacationRequests").Where(u => u.Email == epost).FirstOrDefault();
            if (user.IsAdmin)
            {
                Setting set = settingContext.settings.Where(u => u.Id == set_id).FirstOrDefault();
                //items.FirstOrDefault(t => t.Id == id);
                if (set == null)
                {
                    return new NoContentResult();
                }

                var dbEntityEntry = settingContext.Entry(set);
                await settingContext.SaveChangesAsync();
                return new OkObjectResult(JsonConvert.SerializeObject(set, Formatting.Indented, new JsonSerializerSettings()
                {
                    ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                    PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
                }
                ));
            }
            else
            {
                return new StatusCodeResult(403);
            }
        }
    }
}
