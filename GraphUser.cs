﻿using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace TidsbankenFuncs
{
    class GraphUser
    {
        public bool accountEnabled { get; set; }
        public string surname { get; set; }
        public string givenName { get; set; }
        public string displayName { get; set; }
        public string mailNickname { get; set; }
        public string userPrincipalName { get; set; }
    }

    class GraphResponse
    {
        public List<string> BusinessPhones { get; set; }
        public string DisplayName { get; set; }
        public string GivenName { get; set; }
        public string JobTitle { get; set; }
        public string Mail { get; set; }
        public string MobilePhone { get; set; }
        public string OfficeLocation { get; set; }
        public string PreferredLanguage { get; set; }
        public string Surname { get; set; }
        public string UserPrincipalName { get; set; }
        public string Id { get; set; }
    }

    class RoleResponse
    {
        public List<Role> Value { get; set; }
    }

    class Role
    {
        public string Id { get; set; }
        public string PrincipalId { get; set; }
        public string ResourceScope { get; set; }
        public string DirectoryScopeId { get; set; }
        public string RoleDefinitionId { get; set; }
    }

    class PasswordUpdate
    {
        public string Current { get; set; }
        public string newPass { get; set; }
    }
}