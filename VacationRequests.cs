﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using TidsbankenFuncs.Models;
using System.Net.Http;
using System.Security.Claims;

namespace TidsbankenFuncs
{
    public class VacationRequests
    {
        private const string Route = "request";
        private readonly System.Net.Http.HttpClient vacClient;
        private readonly TidsbankenFuncsDbContext vacContext;
        static List<VacationRequest> items = new List<VacationRequest>();
        static List<Object> results = new List<Object>();

        public VacationRequests(IHttpClientFactory httpClientFactory, TidsbankenFuncsDbContext vacContext)
        {
            this.vacClient = httpClientFactory.CreateClient();
            this.vacContext = vacContext;
        }

        [FunctionName("CreateVacationRequest")]
        public async Task<IActionResult> CreateVacationRequest([HttpTrigger(AuthorizationLevel.Anonymous,
            "post", Route = Route)]HttpRequest req, ILogger log, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            bool userIsCorrect = controlUser.idMatchingDbId(vacContext, Int32.Parse(req.Headers["UserId"]));
            bool isAdmin = controlUser.isUserAdmin(vacContext);

            if (!userIsCorrect && !isAdmin) // If a non-admin tries to delete another profile than itself
            {
                log.LogInformation("UserId input in request does not match user making request");
                return new StatusCodeResult(403);
            }
            log.LogInformation("Creating a new Vacation Request");
            var vac = new VacationRequest
            {
                UserId = Int32.Parse(req.Headers["UserId"]),
                Title = req.Headers["Title"],
                Status = "Pending",
                PeriodStart = DateTime.Parse(req.Headers["PeriodStart"]),
                PeriodEnd = DateTime.Parse(req.Headers["PeriodEnd"])
            };

            await vacContext.vacationRequest.AddAsync(vac);
            await vacContext.SaveChangesAsync();
            return new CreatedResult(Route + "/" + vac.Id, controlUser.serializeObject<VacationRequest>(vac));
        }

        [FunctionName("GetVacationRequest")]
        public async Task<IActionResult> GetVacationRequest([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = Route)]HttpRequest req, ILogger log, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            User currentUser = vacContext.user.Where(u => u.Email == controlUser.epost).FirstOrDefault();
            bool isAdmin = controlUser.isUserAdmin(vacContext);
            log.LogInformation("Getting vacation request list items");
            if (isAdmin)
            {
                items = await vacContext.vacationRequest.Include("Comments").Include("Moderator").Include("User").ToListAsync();
            }
            else
            {
                items = await vacContext.vacationRequest.Include("Comments").Include("Moderator")
                    .Where(vr => (vr.UserId == currentUser.Id || vr.Status == "Approved")).ToListAsync();
            }
            return new OkObjectResult(controlUser.serializeObjectList<VacationRequest>(items));

        }

        [FunctionName("GetVacationRequestById")]
        public IActionResult GetVacationRequestById([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = Route + "/{request_id}")]HttpRequest req, ILogger log, int request_id, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            User currentUser = vacContext.user.Where(u => u.Email == controlUser.epost).FirstOrDefault();
            log.LogInformation(request_id.ToString());
            log.LogInformation(controlUser.serializeObject(currentUser));
            VacationRequest vac = vacContext.vacationRequest.Include("Comments").Include("Moderator").Where(u => (u.Id == request_id)).FirstOrDefault();
            bool isAdmin = controlUser.isUserAdmin(vacContext);
            log.LogInformation(isAdmin.ToString());
            log.LogInformation(vac.Status);
            if (vac == null)
            {
                return new NoContentResult();
            }
            if (currentUser.Id == vac.UserId || isAdmin || vac.Status.Equals("Approved"))
            {
                return new OkObjectResult(controlUser.serializeObject<VacationRequest>(vac));
            } else
            {
                log.LogInformation("Non-admin which does not own VacationRequest is the making request");
                return new StatusCodeResult(403);
            }

        }
        [FunctionName("UpdateVacationRequest")]
        public async Task<IActionResult> UpdateVacationRequest([HttpTrigger(AuthorizationLevel.Anonymous, "patch",
            Route = Route + "/{request_id}")]HttpRequest req, ILogger log, int request_id, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            User currentUser = vacContext.user.Where(u => u.Email == controlUser.epost).FirstOrDefault();
            VacationRequest currentVac = vacContext.vacationRequest.Where(vr => vr.Id == request_id).FirstOrDefault();
            bool isAdmin = controlUser.isUserAdmin(vacContext);
            log.LogInformation("Currentuser.Id = " + currentUser.Id);
            log.LogInformation("VacationUser.Id = " + currentVac.UserId);
            if (currentUser.Id != currentVac.UserId && !isAdmin)
            {
                log.LogInformation("Non-admin which does not own VacationRequest is the making request");
                return new StatusCodeResult(403);
            }

            VacationRequest vac = vacContext.vacationRequest.Include("Comments").Include("Moderator")
                .Where(u => u.Id == request_id).FirstOrDefault();
            //items.FirstOrDefault(t => t.Id == id);
            if (vac == null)
            {
                return new NoContentResult();
            }

            int tempId = Int32.Parse(req.Headers["UserId"]);
            bool isMod = false;
            if (tempId != vac.UserId)
            {
                Moderator mod = vacContext.moderator.Where(m => m.UserId == tempId).FirstOrDefault();
                if (mod != null)
                {
                    vac.ModeratorId = mod.Id;
                    isMod = true;
                    vac.Status = req.Headers["Status"];
                }
            }
            if (currentUser.Moderator != null)
            {
                isMod = true;
            }
            if (string.IsNullOrEmpty(req.Headers["Status"]) && isMod == false)
            {
                return new StatusCodeResult(403);
            }
               
            vac.PeriodStart = DateTime.Parse(req.Headers["PeriodStart"]);
            vac.PeriodEnd = DateTime.Parse(req.Headers["PeriodEnd"]);
            vac.Title = req.Headers["Title"];
            vac.TimeOfModeration = DateTime.Now;
            

            var dbEntityEntry = vacContext.Entry(vac);
            await vacContext.SaveChangesAsync();

            return new OkObjectResult(controlUser.serializeObject<VacationRequest>(vac));
        }

        [FunctionName("DeleteVacationRequest")]
        public async Task<IActionResult> DeleteVacationRequest([HttpTrigger(AuthorizationLevel.Anonymous, "delete",
            Route = Route + "/{request_id}")]HttpRequest req, ILogger log, int request_id, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            bool isAdmin = controlUser.isUserAdmin(vacContext);

            if (!isAdmin) // If a non-admin tries to delete request
            {
                log.LogInformation("Non-admin making request");
                return new StatusCodeResult(403);
            }

            VacationRequest vac = vacContext.vacationRequest.Include("Comments").Where(u => u.Id == request_id).FirstOrDefault();

            if (vac.Comments != null)
            {
                    foreach (Comment com in vac.Comments)
                    {
                        vacContext.comment.Remove(com);
                    }
                
            }
            if (vac == null)
            {
                return new NoContentResult();
            }
            vacContext.vacationRequest.Remove(vac);
            await vacContext.SaveChangesAsync();
            return new OkObjectResult("VacationRequest with ID: " + vac.Id + " has been deleted");
        }
    }
}
