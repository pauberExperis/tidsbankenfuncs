﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFuncs.Models
{
    public class UserIneligiblePeriod
    {
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int IneligiblePeriodId { get; set; }
        public virtual IneligiblePeriod IneligiblePeriod { get; set; }
    }
}
