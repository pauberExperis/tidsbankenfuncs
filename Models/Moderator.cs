﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFuncs.Models
{
    public class Moderator
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public virtual ICollection<IneligiblePeriod>? IneligiblePeriods { get; set; }
        public virtual ICollection<VacationRequest>? VacationRequests { get; set; }
    }
}
