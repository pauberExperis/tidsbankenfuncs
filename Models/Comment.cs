﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFuncs.Models
{
    public class Comment
    {
        public string Message { get; set; }
        public DateTime Created { get; set; }
        public DateTime LastEdited { get; set; }
        public virtual User User { get; set; }
        public int UserId { get; set; }
        public virtual VacationRequest VacationRequest { get; set; }
        public int VacationRequestId { get; set; }
    }
}
