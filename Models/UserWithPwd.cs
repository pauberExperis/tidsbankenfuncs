﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFuncs.Models
{
    public class UserWithPwd
    {
        public int Id { get; set; }
        public string ProfilePic { get; set; }
        public bool IsAdmin { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Pwd { get; set; }
        public virtual ICollection<Comment> Comments { get; set; }
        public virtual ICollection<UserIneligiblePeriod> UserIneligiblePeriods { get; set; }
        public virtual ICollection<VacationRequest> VacationRequests { get; set; }
        public virtual Moderator? Moderator { get; set; }
    }
}
