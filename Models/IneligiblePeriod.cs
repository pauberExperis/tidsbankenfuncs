﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFuncs.Models
{
    public class IneligiblePeriod
    {
        public int Id { get; set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        public int ModeratorId{ get; set; }
        public virtual Moderator Moderator { get; set; }
        public virtual ICollection<UserIneligiblePeriod> UserIneligiblePeriods { get; set; }
    }
    
}
