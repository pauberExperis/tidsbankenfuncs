﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TidsbankenFuncs.Models
{
    public class Setting
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int Value { get; set; }
    }
}
