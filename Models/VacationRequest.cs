﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TidsbankenFuncs.Models
{
    public class VacationRequest
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public DateTime PeriodStart { get; set; }
        public DateTime PeriodEnd { get; set; }
        public DateTime TimeOfModeration { get; set; }
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public virtual Moderator? Moderator { get; set; }
        public int? ModeratorId { get; set; }
        public string Status { get; set; } = "Pending";
        public virtual ICollection<Comment> Comments { get; set; }



    }
}
