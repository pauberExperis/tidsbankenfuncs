﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using TidsbankenFuncs.Models;

namespace TidsbankenFuncs
{
    public class TidsbankenFuncsDbContext : DbContext
    {
        public TidsbankenFuncsDbContext(DbContextOptions<TidsbankenFuncsDbContext> options) : base(options) { }

        public DbSet<User> user { get; set; }
        public DbSet<Moderator> moderator { get; set; }
        public DbSet<Comment> comment { get; set; }
        public DbSet<IneligiblePeriod> ineligiblePeriod { get; set; }
        public DbSet<VacationRequest> vacationRequest { get; set; }
        public DbSet<UserIneligiblePeriod> userIneligiblePeriod { get; set; }
        public DbSet<Setting> settings { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>().HasIndex(u => u.Email).IsUnique();
            modelBuilder.Entity<Comment>().HasKey(c => new { c.UserId, c.VacationRequestId });
            modelBuilder.Entity<UserIneligiblePeriod>().HasKey(ui => new { ui.UserId, ui.IneligiblePeriodId });
            modelBuilder.Entity<UserIneligiblePeriod>().HasOne(x => x.User).WithMany(x => x.UserIneligiblePeriods).HasForeignKey("UserId").OnDelete(DeleteBehavior.Restrict);
            modelBuilder.Entity<Comment>().HasOne(x => x.VacationRequest).WithMany(x => x.Comments).HasForeignKey("VacationRequestId").OnDelete(DeleteBehavior.Restrict);


        }
    }
}
