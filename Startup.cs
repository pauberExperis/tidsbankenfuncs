﻿using Microsoft.Azure.Functions.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using System;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;

[assembly: FunctionsStartup(typeof(TidsbankenFuncs.Startup))]

namespace TidsbankenFuncs
{
    public class Startup : FunctionsStartup
    {

        public override void Configure(IFunctionsHostBuilder builder)
        {

            IConfigurationRoot configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("local.settings.json", optional: true)
                .Build();
            string conn = configuration.GetConnectionString("SqlConnectionString");
            
            if (string.IsNullOrEmpty(conn))
            {
                conn = System.Environment.GetEnvironmentVariable($"SQLAZURECONNSTR_SqlConnectionString", EnvironmentVariableTarget.Process);
            }
            string connParameter = "SqlConnectionString";
            string Prefix = "SQLAZURECONNSTR_";
            string connectionString = System.Environment.GetEnvironmentVariable($"ConnectionStrings:{connParameter}", EnvironmentVariableTarget.Process);
            if (string.IsNullOrEmpty(connectionString))
            {
                connectionString = System.Environment.GetEnvironmentVariable($"{Prefix}{connParameter}", EnvironmentVariableTarget.Process);
            }
            //System.Environment.GetEnvironmentVariable("CONNSTR_Tidsbanken", EnvironmentVariableTarget.Process);
            JsonConvert.DefaultSettings = () => new JsonSerializerSettings
            {
                Formatting = Formatting.Indented,
                PreserveReferencesHandling = PreserveReferencesHandling.Objects,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            builder.Services.AddDbContext<TidsbankenFuncsDbContext>(
                options => options
                //.UseLazyLoadingProxies()
                .UseSqlServer(conn));
            builder.Services.AddHttpClient();
        }
    }
}
