﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Azure.WebJobs;
using Microsoft.Azure.WebJobs.Extensions.Http;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using TidsbankenFuncs.Models;
using System.Net.Http;
using System.Security.Claims;

namespace TidsbankenFuncs
{
    public class UserIneligiblePeriods
    {
        private const string Route = "userineligibleperiod";
        private readonly System.Net.Http.HttpClient ineClient;
        private readonly TidsbankenFuncsDbContext ineContext;
        static List<UserIneligiblePeriod> items = new List<UserIneligiblePeriod>();
        static List<Object> results = new List<Object>();

        public UserIneligiblePeriods(IHttpClientFactory httpClientFactory, TidsbankenFuncsDbContext ineContext)
        {
            this.ineClient = httpClientFactory.CreateClient();
            this.ineContext = ineContext;
        }

        [FunctionName("CreateUserIneligiblePeriods")]
        public async Task<IActionResult> CreateUserIneligiblePeriods([HttpTrigger(AuthorizationLevel.Anonymous,
            "post", Route = Route)]HttpRequest req, ILogger log, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            bool isAdmin = controlUser.isUserAdmin(ineContext);

            if (!isAdmin)
            {
                log.LogInformation("Non-admin which does not own VacationRequest is the making request");
                return new StatusCodeResult(403);
            }
            log.LogInformation("Creating a new User Ineligible Period");
            log.LogInformation(req.Headers["UserId"]);
            var ine = new UserIneligiblePeriod
            {
                IneligiblePeriodId = Int32.Parse(req.Headers["IneligiblePeriodId"]),
                UserId = Int32.Parse(req.Headers["UserId"])
            };

           await ineContext.userIneligiblePeriod.AddAsync(ine);
            await ineContext.SaveChangesAsync();
            return new OkObjectResult(JsonConvert.SerializeObject(ine, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
            }
            ));
        }
       /* [FunctionName("GetUserIneligiblePeriod")]
        public async Task<IActionResult> GetUserIneligiblePeriod([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = Route)]HttpRequest req, ILogger log)
        {

            log.LogInformation("Getting ineligible period list items");
            items = await ineContext.userIneligiblePeriod.Include("User").Include("IneligiblePeriod")./*Include("Comments").Include("Moderator").Include("UserIneligiblePeriods").Include("VacationRequests").*///ToListAsync();
           /* return new OkObjectResult(JsonConvert.SerializeObject(items, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
            }
            ));

        }*/


        /*[FunctionName("GetUserIneligiblePeriodById")]
        public IActionResult GetUserIneligiblePeriodById([HttpTrigger(AuthorizationLevel.Anonymous, "get",
            Route = Route + "/{id}")]HttpRequest req, ILogger log, int id)
        {
            UserIneligiblePeriod ine = ineContext.userIneligiblePeriod.Include("User").Include("IneligiblePeriod").Where(u => (u.UserId == id)).FirstOrDefault();
            if (ine == null)
            {
                return new NotFoundResult();
            }
            return new OkObjectResult(JsonConvert.SerializeObject(ine, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
            }
            ));
        }*/
        /*[FunctionName("UpdateUserIneligiblePeriod")]
        public async Task<IActionResult> UpdateUserIneligiblePeriod([HttpTrigger(AuthorizationLevel.Anonymous, "patch",
            Route = Route + "/{id}")]HttpRequest req, ILogger log, int id, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            bool isAdmin = controlUser.isUserAdmin(ineContext);

            if (!isAdmin)
            {
                log.LogInformation("Non-admin which does not own VacationRequest is the making request");
                return new StatusCodeResult(403);
            }
            UserIneligiblePeriod ine = ineContext.userIneligiblePeriod.Include("User").Include("IneligiblePeriod").Where(u => u.UserId == id).FirstOrDefault();
            //items.FirstOrDefault(t => t.Id == id);
            if (ine == null)
            {
                return new NotFoundResult();
            }

            string requestBody = await new StreamReader(req.Body).ReadToEndAsync();
            var updated = JsonConvert.DeserializeObject<Models.UserIneligiblePeriod>(requestBody);

            ine.UserId = Int32.Parse(req.Headers["UserId"]);
                
            ine.IneligiblePeriodId = Int32.Parse(req.Headers["IneligiblePeriodId"]);

            var dbEntityEntry = ineContext.Entry(ine);
            await ineContext.SaveChangesAsync();

            return new OkObjectResult(JsonConvert.SerializeObject(ine, Formatting.Indented, new JsonSerializerSettings()
            {
                ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore,
                PreserveReferencesHandling = Newtonsoft.Json.PreserveReferencesHandling.Objects
            }
            ));
        }*/

       /* [FunctionName("DeleteUserIneligiblePeriod")]
        public async Task<IActionResult> DeleteUserIneligiblePeriod([HttpTrigger(AuthorizationLevel.Anonymous, "delete",
            Route = Route + "/{id}")]HttpRequest req, ILogger log, int id, ClaimsPrincipal claimsPrincipal)
        {
            ControlUserClass controlUser = new ControlUserClass(claimsPrincipal);
            bool isAdmin = controlUser.isUserAdmin(ineContext);

            if (!isAdmin)
            {
                log.LogInformation("Non-admin which does not own VacationRequest is the making request");
                return new StatusCodeResult(403);
            }
            UserIneligiblePeriod ine = ineContext.userIneligiblePeriod.Where(u => u.UserId == id).FirstOrDefault();
            if (ine == null)
            {
                return new NotFoundResult();
            }
            ineContext.userIneligiblePeriod.Remove(ine);
            await ineContext.SaveChangesAsync();
            return new OkObjectResult("UserIneligiblePeriod with ID: " + ine.UserId + " has been deleted");
        }*/
    }
}
